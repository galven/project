import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, map, flatMap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AdsService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  adCollection:AngularFirestoreCollection


  getAds(userId): Observable<any[]> {
    this.adCollection = this.db.collection(`users/${userId}/ads`);
    return this.adCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data;
      }))
    ); 
  } 


  getAd(userId, id:string):Observable<any>{
    return this.db.doc(`users/${userId}/ads/${id}`).get();
  }
  
  addAd(userId:string, title:string, body:string, type:string,sent:boolean, choose:boolean,time:Date,picid:number){
    const ad = {title:title,body:body,type:type,sent:sent,choose:choose,time:time,picid:picid}
    this.userCollection.doc(userId).collection('ads').add(ad);
  } 

  updateAd(userId:string, id:string,type:string){
    this.db.doc(`users/${userId}/ads/${id}`).update(
      {
        type:type
      }
    )
  }

  updateAdSent(userId:string, id:string,sent:boolean,time:Date){
    this.db.doc(`users/${userId}/ads/${id}`).update(
      {
        sent:sent,time:time
      }
    )
  }

  updatePhotoChoose(userId:string, id:string,choose:boolean,picid:number){
    this.db.doc(`users/${userId}/ads/${id}`).update(
      {
        choose:choose,picid:picid
      }
    )

  }
  
  deleteAd(id:string,userId:string){
    this.db.doc(`users/${userId}/ads/${id}`).delete();
  }


  constructor(private db: AngularFirestore, private authService:AuthService,) { }
}