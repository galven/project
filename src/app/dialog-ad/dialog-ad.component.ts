import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';
import { AdsService } from './../ads.service';
import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { inject } from '@angular/core/testing';
import { PhotosService } from '../photos.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-dialog-ad',
  templateUrl: './dialog-ad.component.html',
  styleUrls: ['./dialog-ad.component.css']
})
export class DialogAdComponent implements OnInit {

  body:string;
  userId:string;
  picid:number;
  choose:boolean;
  Photos$: Observable<any>;

  constructor(
    private router:Router ,private route:ActivatedRoute, public afs:AngularFirestore,
    public authService :AuthService, public adservice:AdsService,
    public dialogRef:MatDialogRef<DialogAdComponent>,@Inject(MAT_DIALOG_DATA) public data:any,private photoService: PhotosService) {
      this.body  = data.body;
      this.picid =data.picid;
      this.choose=data.choose;
     }

    


     close():void {
      this.dialogRef.close();
    }
    
  ngOnInit() {
    this.Photos$=this.photoService.getPhoto();


  }
    


}
