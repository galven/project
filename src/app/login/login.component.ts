import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService:AuthService,
    private router:Router) {
      
     }

  email:string;
  password:string; 
  errorMessage:any;
  successMessage:any;

  tryLogin(value){
    this.authService.dologin(value)
    .then(res => {
      console.log(res);
      this.errorMessage = "";
      this.successMessage = "Your Are LogedIn";
    }, err => {
      console.log(err);
      this.errorMessage = err.message;
      this.successMessage = "";
    })
  }




  onSubmit(){
    this.authService.login(this.email,this.password);
  }

  ngOnInit() {
  }

}
