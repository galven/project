import { Photo } from './interface/photo';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  constructor(private _http: HttpClient) { }

  apiUrl = "https://jsonplaceholder.typicode.com/photos"

  getPhoto(){
    return this._http.get<Photo[]>(this.apiUrl);

  }
}
