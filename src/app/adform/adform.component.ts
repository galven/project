import { Component, OnInit } from '@angular/core';
import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adform',
  templateUrl: './adform.component.html',
  styleUrls: ['./adform.component.css']
})
export class AdformComponent implements OnInit {

  url:string;
  text:string;
  title:string;

  onSubmit(){
    this.classifyService.doc = this.text;
    this.classifyService.title = this.title;
    
    this.router.navigate(['/classified']);
  }

  constructor(private classifyService:ClassifyService, 
    public imageService:ImageService,
    private router:Router ) { }


  ngOnInit() {
  }

}
