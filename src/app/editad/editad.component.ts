import { AdsService } from './../ads.service';
import { Component, OnInit } from '@angular/core';
import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-editad',
  templateUrl: './editad.component.html',
  styleUrls: ['./editad.component.css']
})
export class EditadComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string; 
  cn:any;
  cas:object[] = [{id:0, name:'ham'},{id:1, name:'spam'}];
  
  idd: number;
  body:string;
  id:string;
  userId:string;
  title:string;
  change:boolean=false;


  change_cat(){
    this.change=true;
  }
  onSubmit1(){
  
    console.log(this.userId);
    console.log(this.body);
    console.log(this.category);
    this.adservice.updateAd(this.userId,this.id,this.category);
    this.router.navigate(['/ads']);
  }
  onSubmit(){
    if (this.cn === 'ham') {
      this.idd=0;
     // alert(this.id);
    }else if (this.cn === 'spam') {
      this.idd=1;
     // alert(this.id);
    }
    console.log(this.id);
     this.category = this.classifyService.categories[this.idd];
     this.categoryImage = this.imageService.images[this.idd];
  }


  constructor(public classifyService:ClassifyService,
    public imageService:ImageService, 
    private router:Router ,private route:ActivatedRoute,
    public authService :AuthService, public adservice:AdsService) { }

  ngOnInit() {

    this.id=this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user=>{
        this.userId = user.uid;
          this.adservice.getAd(this.userId,this.id).subscribe(
            ad=>{
              this.body=ad.data().body;
              this.title=ad.data().title;
              this.category=ad.data().type;
              if (this.category === 'ham') {
                this.idd=0;
              }else if (this.category === 'spam') {
                this.idd=1;
               // alert(this.id);
              }
              this.categoryImage = this.imageService.images[this.idd];
            }

          ) 
        
          console.log(this.id);
        }

        )
      
    
      }
    
  }


