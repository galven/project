import { DashboardService } from './../dashboard.service';
import { AuthService } from './../auth.service';
import { Component } from '@angular/core';
import { map, count } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Chart } from 'chart.js';
import { AdsService } from '../ads.service';
import { MultiDataSet, Label } from 'ng2-charts';
import { ChartType } from 'chart.js';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  

  constructor(private adservice: AdsService,private breakpointObserver: BreakpointObserver,
     public authservice:AuthService, public dashboardService:DashboardService, private db: AngularFirestore) {

  }

// $categories:Observable<number[]>;
 
userId:string;
userEmail:string;
spam:number;
ham:number;
AdsData:any[];
AdsData1:any[];
clickedPie:boolean = false;
clickedBar:boolean = false;
clickedDoughnut:boolean = false;


// Pie chart
public pieChartLabels:string[] = [];
public pieChartData:number[] = [];
public pieChartType:string = 'pie';
public pieChartColors = [
  {
    backgroundColor:[],
  },
];

 //dounat chart
 doughnutChartLabels: Label[] = [];
 doughnutChartData: MultiDataSet = [];
 doughnutChartType: ChartType = 'doughnut';
 public doughnutChartColors:  any[] = [{ backgroundColor: [] }];


 // bar chart
 public barChartOptions = {scaleShowVerticalLines: false,responsive: true};
 public barChartLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul','Agu','Sep','Oct','Nov','Dec'];
 public barChartType = 'bar';
 public barChartLegend = true;
 public barChartData = [ {data: [], label: ''} ];

 ngOnInit() {
  this.authservice.user.subscribe(
    user => {
      this.userId = user.uid;
      this.userEmail = user.email;
     })
    }


  public count_cat(){
  this.clickedPie = true;
  this.AdsData = [];
  this.adservice.getAds(this.userId).subscribe(data => {
  this.AdsData = data;
  this.processResults();  
});
}

processResults() {

 let spam_num:number=0;
 let ham_num:number=0;
  for (let i = 0; i < this.AdsData.length; i++) {
   
    if(this.AdsData[i].type === 'spam')
    {
      spam_num++
     }
     else 
     {
       ham_num++
     }

  }
   this.addPieData(spam_num,ham_num); 
}

addPieData(spam:number,ham: number){

  this.pieChartData = [];
  this.pieChartLabels = [];
  this.pieChartLabels.push('spam');
  this.pieChartData.push(spam);
  this.pieChartColors[0].backgroundColor.push('rgba(196,0,0,0.3'); 
  this.pieChartLabels.push('ham');
  this.pieChartData.push(ham);
  this.pieChartColors[0].backgroundColor.push('rgba(0,30,246,0.3');

}

//bar chart
getAdsByMonth(){
  this.clickedBar = true;
  this.AdsData1 = [];
  this.adservice.getAds(this.userId).subscribe(data => {
  this.AdsData1 = data;
  console.log(data);
  this.processResults1();  
});
}

processResults1(){
let MonthArr: number[] = [0,0,0,0,0,0,0,0,0,0,0,0,];

for (let i = 0; i < this.AdsData1.length; i++){

  if((this.AdsData1[i].time)!=null){ 
  var date = new Date(this.AdsData1[i].time.seconds *1000);
  // console.log('geting the month');
  // console.log(this.AdsData1[i].time);
  // console.log("getting the date",date);
  // console.log(date.getMonth());
  MonthArr[date.getMonth()]++;
  }
}
//adding the data
console.log('the month arr is ',MonthArr);
  this.barChartData = [];
  this.barChartData.push({data: MonthArr, label: 'my ads'});
}

// doughnutChart
getRandomNumbers(){

  let random1 = 0;
  let random2 = 0;

  random1 = Math.round(Math.random()*100);
  random2 = Math.round(Math.random()*100);

  return [random1,random2];
}

addDountData(){
this.clickedDoughnut = true;
let nums =this.getRandomNumbers();

// clean data of chart
  this.doughnutChartData = [];
  this.doughnutChartLabels = [];
  //insert new data
  this.doughnutChartLabels.push('Not Entered', 'Entered');
  this.doughnutChartData.push(nums);
  this.doughnutChartColors[0].backgroundColor.push('rgba(196,0,0,0.3'); 
  this.doughnutChartColors[0].backgroundColor.push('rgba(0,30,246,0.3');
}

   // events
   public chartClicked(e:any):void {
     console.log(e);
   }
  
   public chartHovered(e:any):void {
     console.log(e);
   }

 //bar chart

 
}
