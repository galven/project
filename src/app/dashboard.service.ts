import { Observable } from 'rxjs';
import { AdsService } from './ads.service';
import { Injectable } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { AuthService } from './auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private adservice: AdsService,
    private breakpointObserver: BreakpointObserver, 
    public authservice:AuthService, 
    private db: AngularFirestore) { }

    database = firebase.database();

}
